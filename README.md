# Llista de Regals

[![pipeline status](https://gitlab.com/rogerclotet/llista-regals/badges/master/pipeline.svg)](https://gitlab.com/rogerclotet/llista-regals/commits/master)
[![Requires.io](https://img.shields.io/requires/enterprise/clotet/llista-regals/master)](https://requires.io/enterprise/clotet/llista-regals/requirements/?branch=master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Development

### Pre-commit hook
```shell script
pip install pre-commit
pre-commit install
```

### Run once to set up the database and admin account
```shell script
docker-compose run --rm web python manage.py migrate
docker-compose run --rm web python manage.py createsuperuser
```

### Run dev stack
```shell script
docker-compose up
```
