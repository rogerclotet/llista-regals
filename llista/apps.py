from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class LlistaConfig(AppConfig):
    name = "llista"
    verbose_name = _("Llista")
