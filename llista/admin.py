from django.contrib import admin

from llista.models import Product, Category


admin.site.register(Category)
admin.site.register(Product)
