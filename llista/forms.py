from django import forms


class ReserveForm(forms.Form):
    product = forms.IntegerField()

    class Meta:
        widgets = {"product": forms.HiddenInput()}
