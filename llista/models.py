from django.db import models
from django.utils.translation import gettext_lazy as _


class Category(models.Model):
    name = models.CharField(max_length=256, verbose_name=_("Nom"))
    icon = models.CharField(max_length=2, blank=True, verbose_name=_("Icona (emoticona)"))
    description = models.TextField(blank=True, verbose_name=_("Descripció"))
    priority = models.IntegerField(default=0, verbose_name=_("Prioritat"))

    def __str__(self):
        if self.icon != "":
            return f"{self.icon} {self.name}"
        return self.name

    class Meta:
        ordering = ("-priority", "name")
        verbose_name = _("Categoria")
        verbose_name_plural = _("Categories")


class Product(models.Model):
    name = models.CharField(max_length=256, verbose_name=_("Nom"))
    description = models.TextField(blank=True, verbose_name=_("Descripció"))
    image_url = models.CharField(max_length=256, blank=True, verbose_name=_("Imatge"))
    price = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True, verbose_name=_("Preu"))
    url = models.CharField(max_length=256, blank=True, verbose_name=_("Enllaç"))

    needed = models.IntegerField(default=1, verbose_name=_("En necessitem"))
    reserved = models.IntegerField(default=0, verbose_name=_("Reservats"))

    category = models.ForeignKey(to=Category, null=True, on_delete=models.CASCADE, verbose_name=_("Categoria"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = (
            "reserved",
            "name",
        )
        verbose_name = _("Producte")
        verbose_name_plural = _("Productes")
