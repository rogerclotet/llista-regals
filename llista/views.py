from django.contrib import messages
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

from llista.forms import ReserveForm
from llista.models import Category, Product


def index(request: HttpRequest) -> HttpResponse:
    categories = Category.objects.all()

    if request.method == "POST":
        reserve_form = ReserveForm(request.POST)
        if reserve_form.is_valid():
            product = Product.objects.get(pk=reserve_form.cleaned_data.get("product"))
            if product.reserved >= product.needed:
                messages.error(
                    request=request,
                    message="Aquest producte ja està reservat. "
                    "Prova-ho amb algun altre o torna a carregar la pàgina, sisplau",
                )
            else:
                product.reserved = product.reserved + 1
                product.save()

                messages.success(request=request, message="Enviat correctament, moltes gràcies!")
        else:
            messages.error(
                request=request,
                message="Hi ha hagut algun problema. Torna a intentar-ho més tard o avisa'ns del problema, sisplau",
            )

    form = ReserveForm()

    return render(request, "index.html", {"categories": categories, "form": form})
